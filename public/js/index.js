let app = angular.module('its2021z',
	[ 'ngRoute', 'ngSanitize', 'ngAnimate', 'ui.bootstrap', 'ngMaterial']
)

app.constant('routes', [
	{ route: '/', templateUrl: 'home.html', controller: 'Home', controllerAs: 'ctrl', title: '<i class="fa fa-lg fa-home"></i>' },
	{ route: '/persons', templateUrl: 'persons.html', controller: 'Persons', controllerAs: 'ctrl', title: 'Osoby' },
	{ route: '/projects', templateUrl: 'projects.html', controller: 'Projects', controllerAs: 'ctrl', title: 'Projekty' },
	{ route: '/tasks', templateUrl: 'tasks.html', controller: 'Tasks', controllerAs: 'ctrl', title: 'Zadania' }
])

// instalacja routera
app.config(['$routeProvider', '$locationProvider', 'routes', '$mdDateLocaleProvider',
	function($routeProvider, $locationProvider, routes, $mdDateLocaleProvider) {
		$locationProvider.hashPrefix('')
		for(var i in routes) {
			$routeProvider.when(routes[i].route, routes[i])
		}
		$routeProvider.otherwise({ redirectTo: '/' })

		$mdDateLocaleProvider.months = ['styczeń', 'luty', 'marzec', 'kwiecień', 'maj', 'czerwiec', 'lipiec', 'sierpień', 'wrzesień', 'październik', 'listopad', 'grudzień'];
		$mdDateLocaleProvider.shortMonths = ['sty', 'lut', 'mar', 'kwi', 'maj', 'cze', 'lip', 'sie', 'wrz', 'paź', 'lis', 'gru'];
		$mdDateLocaleProvider.days = ['niedziela', 'poniedziałek', 'wtorek', 'środa', 'czwartek', 'piątek', 'sobota'];
		$mdDateLocaleProvider.shortDays = ['Nie', 'Pon', 'Wt', 'Śr', 'Czw', 'Pt', 'Sob'];
		$mdDateLocaleProvider.firstDayOfWeek = 1;

		$mdDateLocaleProvider.formatDate = function(date) {
			console.log(date);
			if (date) {
				date = date.toLocaleString().split(',')[0].padStart(10, '0');
			}
			return date;
		};
		$mdDateLocaleProvider.parseDate = function(dateString) {
			console.log(dateString);
			if (dateString) {
				let s = dateString.split('.');
				let d = new Date(s[2] + '-' + s[1] + '-' + s[0] + 'T22:00:00.000Z');
				console.log(d);
				return d;
			}
			return new Date(NaN);
		};
}])

app.controller('Index', [ '$location', '$scope', 'routes', 'common', function($location, $scope, routes, common) {
    let ctrl = this
	console.log('Kontroler Index wystartował')
	
	ctrl.alert = common.alert
	ctrl.closeAlert = common.closeAlert

	ctrl.menu = []

    ctrl.rebuildMenu = function() {
		ctrl.menu.length = 0
		for(var i in routes) {
			ctrl.menu.push({ route: routes[i].route, title: routes[i].title })
		}
		$location.path('/')
    }

    ctrl.navClass = function(page) {
        return page === $location.path() ? 'active' : ''
    }

	ctrl.isCollapsed = true
    $scope.$on('$routeChangeSuccess', function () {
        ctrl.isCollapsed = true
    })

	ctrl.rebuildMenu()

}])

app.service('common', [ function() {
	console.log('Usługa common startuje')
	var common = this

	common.test = function() { console.log('Test') }

	common.alert = { type: 'alert-default', text: '' }
	common.closeAlert = function() { common.alert.text = '' }

}])
